window.App = {};
App.shoppingApp = angular.module('shoppingApp', []);

App.shoppingApp.controller('ShoppingListCtrl', function($scope, $http){
    $http.get('/shopping/list').success(function(data) {
        $scope.list = data;
    });

    $scope.update = function(item) {
        $http.put('/shopping/update', item)
            .success(function(data) {
                console.log(data);
            })
            .error(function(data) {
                alert(data.message);
            });
    }

    $scope.remove = function(items, index) {
        items.splice(index, 1);
        return true;
        $http.put('/shopping/delete', item)
            .success(function(data) {
                console.log(data);
            });
    }
});
