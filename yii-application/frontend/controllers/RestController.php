<?php
/**
 * Created by PhpStorm.
 * User: sanches
 * Date: 02.12.14
 * Time: 18:54
 */

namespace frontend\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\filters\VerbFilter;

class RestController extends ActiveController {

    public $modelClass = 'frontend\models\ListModel';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update' => ['put'],
                ],
            ],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        return true;
    }
}