<?php
/**
 * Created by PhpStorm.
 * User: sanches
 * Date: 30.11.14
 * Time: 13:18
 */

namespace frontend\controllers;

use frontend\models\ListModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ListView;

class ShoppingController  extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update' => ['put'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false; // <-- here
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $list = ListModel::findAll(['user_id'=>\Yii::$app->user->identity->id]);
        return $list;
    }

    public function actionUpdate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = array();
        $post = \Yii::$app->request->rawBody;
        if ($post) {
            $data = json_decode($post, true);
            if (isset($data))
            $model = ListModel::findOne($data['id']);
            $model->setAttributes($data);
            return $model->save();
        }
        return $data;
    }
} 