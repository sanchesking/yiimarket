<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CategorytSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'parent_id',
                'format' => 'html',
                'value' => function ($model, $index, $widget) {
                        return $model->parent_id ? $model->parent->name : 'No parent';
                    },
                'filter' => Category::getSelectList(true)
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
