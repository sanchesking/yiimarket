<?php $this->registerJsFile('/js/angular.js', array('position'=>$this::POS_END)); ?>
<?php $this->registerJsFile('/js/controllers.js', array('position'=>$this::POS_END)); ?>
<div ng-app="shoppingApp">
    <div ng-controller="ShoppingListCtrl">
        <ul>
            <li ng-repeat="item in list">
                <span>{{item.name}}</span> |
                <input type="text" value="{{item.count}}" ng-model="item.count"  ng-change="update(item)"/>
                <a ng-click="remove(list,$index)">X</a>
            </li>
        </ul>
        Items count: {{list.length}}
    </div>
</div>