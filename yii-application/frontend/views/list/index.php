<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="list-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create List Model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'status',
            [
                'attribute' => 'category_id',
                'format' => 'html',
                'value' => function ($model, $index, $widget) {
                        return $model->category ? $model->category->name : 'No category';
                    },
                'filter' => Category::getSelectList()
            ],
            'user_id',
            'count',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
