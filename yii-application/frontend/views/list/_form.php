<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Category;
use frontend\models\User;
use frontend\models\ListModel;

/* @var $this yii\web\View */
/* @var $model app\models\ListModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="list-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'status')->dropDownList(ListModel::getStatusList()) ?>

    <?= $form->field($model, 'category_id')->dropDownList(Category::getSelectList()) ?>

    <?= $form->field($model, 'user_id')->dropDownList(User::getSelectList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
