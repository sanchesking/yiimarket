<?php

use yii\widgets\ListView;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listItem',
    'layout' => '{items}{pager}',
    'itemOptions' => [
    ],
    'options' => [
        'links' => '<h2>test</h2>',
        'tag' => 'ul',
        'class' => 'ten-vertical summary-list',

    ],
]);