<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "list".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $category_id
 * @property integer $user_id
 *
 * @property Category $category
 */
class ListModel extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DELETE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'user_id'], 'required'],
            [['status', 'category_id', 'user_id', 'count'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
            'count' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public static function getStatusList()
    {
        $array = [
            self::STATUS_ACTIVE => 'active',
            self::STATUS_DELETE => 'delete',
        ];
        return $array;
    }
}
