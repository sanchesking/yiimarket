<?php

namespace frontend\models;

use frontend\models\ListModel;
use yii\base\Model;
use Yii;


class ListForm extends Model {

    public $id;
    public $name;
    public $status;
    public $category_id;
    public $user_id;
    public $count;

    public function rules()
    {
        return [
            [['name', 'category_id', 'user_id'], 'required'],
            [['status', 'category_id', 'user_id', 'count'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

} 